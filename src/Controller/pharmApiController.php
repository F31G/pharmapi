<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class pharmApiController
{
    public function nein(): Response
    {
        $number = 'Et non';

        return new Response(
            '<html><body>Lucky number: '.$number.'</body></html>'
        );
    }
}